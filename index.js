const fs = require('fs');
const readFrom = require('path');

function readFiles(fs, path){
    return new Promise((resolve, reject) => {
        fs.readdir(path, (err, files) => {
            if( err ) reject(err); // не прочитать содержимое папки         
            else resolve(files.filter(item => !(/(^|\/)\.[^\/\.]/g).test(item)));//Игнорим системные файлы
        });
    })
}

async function createTreeText (fs, path, flag = '', depth = 1, level = 2, dirCount = 0, fileCount = 0) {

    let kol_dir = dirCount;
    let kol_files = fileCount;
    
    const files = await readFiles(fs, path).catch(err => console.log(err));
    for(let key in files){
        var filePath = path + '//' + files[key];
        var stat = fs.statSync(filePath);        
        if (stat.isFile()) {
            kol_files ++;
            printTree(files[key], level);
        }
        else if (stat.isDirectory) {
            kol_dir++;
            printTree(files[key], level);
            if( level <= depth || flag == '') //если указан флаг -d и глубина, то ограничиваемся по level, иначе смотрим всю глубину
            {
                let { kol_dir: returnedCntDir, kol_files: returnedCntFiles } = await createTreeText(fs, path + '/' + files[key], flag, depth, level + 1) 
                kol_files += returnedCntFiles;
                kol_dir += returnedCntDir;
            }
            else {
                return {kol_dir, kol_files}
            }
        }
    }
    return {kol_dir, kol_files}
}
function printTree(strToPrint, level) {
    console.log( level > 1  ? (level == 2 ? '├' + '─'  + strToPrint : '│' + ' '.repeat( level - 2 ) + '└' + '─' + strToPrint) : strToPrint );
}

/* ждем результат*/
let bildTree = async () => {
    let {kol_dir, kol_files} = await createTreeText(fs, process.argv[2], process.argv[3], process.argv[4]);
    //let {kol_dir, kol_files} = await createTreeText(fs, './node', '-d', 3);  //для тестирования упрощение ввода пар-ров
    console.log(`${kol_dir} directories, ${kol_files} files`);
}


bildTree(); // Вызов 
